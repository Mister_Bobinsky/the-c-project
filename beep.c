/* The stdio.h header defines three variable types, several 
macros, and various functions for performing input and output.

The windows.h header defines functions within the Windows API, 
informally WinAPI, which is Microsoft's core set of application programming 
interfaces (APIs) available in the Microsoft Windows operating systems.*/

#include <stdio.h>
#include <windows.h>

int main() {
    int user_response;

    printf("Enter '1' to play BEEP sound!\n");
    scanf("%d", &user_response);

    /* Beep Function: Beep([Frequency], [Time]) 
    Example: Beep(400, 500) == Frequency 400 for 0.5 seconds */
    if(user_response == 1) {
        Beep(450, 550);
        Sleep(10);
        Beep(500, 550);
        Sleep(10);
        Beep(550, 550);
        Sleep(10);

        Beep(500, 250);
        Sleep(1);
        Beep(475, 300);
        Sleep(5);

        Beep(575, 225);
        Sleep(5);
        Beep(580, 225);
        Sleep(5);
        Beep(610, 300);
        Sleep(30);

        Beep(450, 800);
        Sleep(5);

        Beep(600, 225);
        Sleep(1);
        Beep(600, 600);
        Sleep(1);
    } else {
        printf("The requested user response was not entered!\n");
    }

    return 0;
}