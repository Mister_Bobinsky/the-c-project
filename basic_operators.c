/* The stdio.h header defines three variable types, several 
macros, and various functions for performing input and output.

The stdlib.h and time.h headers are used for generating random numbers. */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Function for ranking priority of C operators with a simple random number generator at the bottom to showcase usage of if... else statements */

int main() {

    const char modulo = '%';

    /* For loop containing switch statements ranking the priority of C operators from highest to lowest priority */

    for(int i = 0; i < 10; i++) {
        switch(i) {
            case 1:
                printf("%d\t\t++ -- ()\n", i);
                break;
            case 2:
                printf("%d\t\t! (typecast)\n", i);
                break;
            case 3:
                printf("%d\t\t* / %c\n", i, modulo);
                break;
            case 4:
                printf("%d\t\t+ -\n", i);
                break;
            case 5:
                printf("%d\t\t< <= > >=\n", i);\
                break;
            case 6:
                printf("%d\t\t== !=\n", i);
                break;
            case 7:
                printf("%d\t\t&&\n", i);
                break;
            case 8:
                printf("%d\t\t||\n", i);
                break;
            case 9:
                printf("%d\t\tall assignment operators\n", i);
                break;
            default:
                printf("Priority\tSymbol\n");
                break;
        }
    }

    srand (time(NULL)); // Seeding Rand Generator
    int a = rand();     // Generating random number for variable 'a'
    int b = rand();     // Generating random number for variable 'b'

    if(a == b) {
        printf("\nVariable a is == variable b!\n");
    } else if(a < b) {
        printf("\nVariable a is < variable b!\n");
    } else {
        printf("\nVariable a is > variable b!\n");
    }

    printf("Variable a = %d | Variable b = %d\n", a, b);

    // Note #1: Shorthand Notation -> ++ -- *= /= %= += -=
    // Note #2: Comparisons -> && || !=

    return 0;
}