/* The stdio.h header defines three variable types, several 
macros, and various functions for performing input and output. */

#include <stdio.h>

/* Function manipulating various variables */

int main() {
    
    /* Declaring various variables */

    const int LOOPS = 10;               // Constant variable used for loops
    int counter = 0;                    // Counter variable used within for loop
    
    int wholeNumber;                    // Declaring int variable: wholeNumber
    float decimalSmall;                 // Declaring float variable: decimalSmall
    double decimalLarge;                // Declaring double variable: decimalLarge
    char character;                     // Declaring char variable: character

    wholeNumber = 7;                    // A whole number
    decimalSmall = 3.141592;            // 6 Decimal Places
    decimalLarge = 2.577215664901532;   // 15 Decimal Places
    character = 'R';                    // A Character

    /* Varibles used to demonstrate casting types */
    double lightVar = 1.86;
    int castExample;

    int sourceInt = 65;
    char targetChar;

    castExample = (int)lightVar;
    targetChar = (char)sourceInt;

    /* Printing varibles */

    printf("This is an int: \t%d\n", wholeNumber);
    printf("This is a float: \t%3.2f\n", decimalSmall);
    printf("This is a double: \t%lf\n", decimalLarge);
    printf("This is a char: \t%c\n", character);

    /* For Loop Example */

    for(int i = 0; i < LOOPS; i++) {
        counter++;
    }

    /* Printing casted varibles */

    printf("Counter set: \t\t%d\n", counter);
    printf("This is a cast example using int: \t%d\n", castExample);
    printf("This is a cast example using char: \t%c\n", targetChar);

    return 0;
}