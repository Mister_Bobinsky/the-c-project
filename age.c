#include <stdio.h>

int main()
{
   char name[30];
   int age;
   printf("Enter your first name: \n");
   scanf("%s", &name);
   printf("Enter your age: \n");
   scanf("%d", &age);
   printf("Hello, my name is %s and I'm %d years old.\n", name, age);
   return 0;
}