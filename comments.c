/* The stdio.h header defines three variable types, several 
macros, and various functions for performing input and output. */

#include <stdio.h>

/* Function to remind User how to compile and run C programs. */

int main() {
    char compileCommand[] = "gcc filename.c -o filename";               // Compile Command
    char runCommand[] = "filename";                                     // Run Command
    printf("Compile C program using command: %s\n", compileCommand);    // Print statement for compile command
    printf("Run C program using command: .%c%s\n", 92, runCommand);     // Print statement for run command
    return 0;
}